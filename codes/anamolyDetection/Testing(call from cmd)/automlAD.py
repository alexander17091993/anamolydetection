
# coding: utf-8

# In[11]:


import scipy.io as sp
import pandas as pd
import numpy as np
from sklearn.ensemble import IsolationForest 
import matplotlib.pyplot as plt
import os 
from os import listdir
import sys


# In[3]:


def read_input(pwd, filename, header_info):
    if filename.endswith ('.mat'):
        dict_ = sp.loadmat(pwd+filename)
        return convert_dic_to_df(dict_, 'X', 'y')#TODO automate this step
    elif filename.endswith ('.data'):
        df = pd.read_csv(pwd+filename , header= header_info )
        return df


# In[4]:


def convert_dic_to_df(dict_, X_label, Y_label):
    df1 = pd.DataFrame(dict_[X_label])
    df2 = pd.DataFrame(dict_[Y_label])
    df_X = pd.DataFrame(df1.values, columns = np.arange(df1.shape[1]))
    df_Y = pd.DataFrame(df2.values, columns = np.arange(df2.shape[1]))
    frames=[df_X,df_Y]
    df = pd.concat(frames, axis=1) 
    return df    


# In[5]:


def driver(pwd,filename, header_info):
    df= read_input(pwd,filename, header_info)
    return df


# In[6]:


def isofor(df, n_est):
    isolation_forest = IsolationForest(n_estimators = n_est)
    isolation_forest.fit(df)
    anomaly_scores = isolation_forest.decision_function(df)
    sorted_anomaly_scores = np.sort(anomaly_scores)
    outliers = isolation_forest.predict(df)
    return anomaly_scores, outliers


# In[7]:


def preprocessing_predictions_actuals(outliers, df):
    for i in range(len(outliers)):
        if outliers[i]==1:
            outliers[i]=0
        if outliers[i]==-1:
            outliers[i]=1
    actuals = df.iloc[:,-1]
    predictions = outliers
    return predictions, actuals


# In[8]:


def evaluate(predictions, actuals, beta):# both arrays are either 1 and 0 valued arrays, 0 normal, 1 anomaly
    TP = 0.0
    FN = 0.0
    FP = 0.0
    TN = 0.0
    for i in range(len(actuals)):#put checks in preprocessing for length of actual predicted to match 
        if predictions[i] == 1 and actuals[i] == 1:#True positive
            TP += 1
        elif actuals[i] == 1 and predictions[i] == 0:#False Negative
            FN += 1
        elif actuals[i] == 0 and predictions[i] == 1: #False Positives
            FP += 1
        elif actuals[i] == 0 and predictions[i] == 0: # True Negatives
            TN += 1
    recall = TP/(TP+FN)
    precision = TP/(TP+FP)
    f_score = ((1+beta)**2*recall*precision)/((beta**2)*recall+ precision)
    return recall, precision, f_score


# In[9]:


def sub_main(pwd, filename, header_info, y_label):
    df=driver(pwd,filename, header_info)
    anomaly_scores, outliers = isofor(df,100)
    predictions,actuals = preprocessing_predictions_actuals(outliers, df)
    recall, precision, f_score = evaluate(predictions,actuals,5)
    return recall, precision, f_score


# In[13]:


if __name__== "__main__":
    files = os.listdir(sys.argv[1])
    performances=[]
    for i in range(len(files)): 
        performances.append(sub_main(sys.argv[1], files[i], None, 0))
    print performances

